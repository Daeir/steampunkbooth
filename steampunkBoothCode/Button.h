class Button{

	int sensorPin;

	int buttonStateFinal;
	int lastSensorState;
	int reading;
	int lastReading;

	unsigned long lastDebounceTime;
	unsigned long debounceDelay;

	public:
	Button( int sensorPin_){

		sensorPin = sensorPin_;

		buttonStateFinal = HIGH;
		lastSensorState = HIGH;

		lastDebounceTime = 0;
		debounceDelay = 50;

		pinMode(sensorPin, INPUT_PULLUP);
	}

	int Update(){
		//read the sensorState
		reading = digitalRead(sensorPin);
		//check to see if you just pressed the button
		if( reading != lastReading){
			//record the time
			lastDebounceTime = millis();
		}
	
		if (( millis() - lastDebounceTime) > debounceDelay) {
			//if the button has been pressed longer then the delay 
			//(so a human pressed the button and its not bouncing)

			if(reading != lastSensorState){
				//if the reading is different after debouncing
				if(reading == HIGH){

					buttonStateFinal = HIGH;
					
				}
				else if(reading == LOW){

					buttonStateFinal = LOW;
					Keyboard.print('g');
				}
				
			}
			lastSensorState = reading;
		}

		lastReading = reading;

		return buttonStateFinal;
	}
};
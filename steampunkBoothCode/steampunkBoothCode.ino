#include <Adafruit_NeoPixel.h>
#include "Button.h"

const int LEDPINL = 3;
const int LEDPINR = 4;
const int LEDAMOUNT = 114;
int hue[LEDAMOUNT];

const int BTNLEDPIN = 2;
const int BTNLEDAMOUNT = 7;
int hueBtn[BTNLEDAMOUNT];

Adafruit_NeoPixel stripL(LEDAMOUNT, LEDPINL, NEO_BRG + NEO_KHZ800);
Adafruit_NeoPixel stripR(LEDAMOUNT, LEDPINR, NEO_BRG + NEO_KHZ800);
Adafruit_NeoPixel btnLeds(BTNLEDAMOUNT, BTNLEDPIN, NEO_GRB + NEO_KHZ800);

//this controls the "framerate" of the animation bigger number is a lower "framerate"
const uint32_t interval = 50;
uint32_t previousTime = 0;

Button pushButton(1);

void setup() {

	Serial.begin(115200);

	//initialize ledstrips and turn them to black
	stripL.begin();
	stripL.show();

	stripR.begin();
	stripR.show();

	btnLeds.begin();
	btnLeds.show();

	//generate hue amounts for the tubes based on a sine function 
	//should be values from 170 to 230 in a sine pattern
	//you can tinker with these values to change the animation
	int s = 0;
	for(int i=0; i<LEDAMOUNT; i++){

		hue[i] = 30*sin(0.1 * s)+ 200;
		s+=1; 
	}

	s = 0;
	for(int i=0; i<BTNLEDAMOUNT; i++){

		hueBtn[i] = 30*sin(s)+ 200;
		s+=1; 
	}

}

void loop() {

	uint32_t currentTime = millis();
	if(currentTime - previousTime > interval){
		//this is a timer that doesnt block the arduino
		shiftHueTable();
		applyColors();

		shiftBtnHueTable();
		applyBtnLedColor();
		//applyBtnLedBreathing();
		previousTime = currentTime;
	}

	pushButton.Update();
}

void applyBtnLedColor(){
	//this animation does a turning with light
	for(int i=0; i<BTNLEDAMOUNT; i++){
		
		btnLeds.setPixelColor(i,convertColor(hueBtn[i], 100, 10));
	}
	btnLeds.show();
}

void applyBtnLedBreathing(){
	//this animation is in sync with the tubes and does a breathing animation
	for(int i=0; i<BTNLEDAMOUNT; i++){
		
		btnLeds.setPixelColor(i,convertColor(hue[1], 100, 10));
	}
	btnLeds.show();
}


void applyColors(){

	for(int i=0; i<LEDAMOUNT; i++){
	
		stripL.setPixelColor(i, convertColor(hue[i], 100, 10));
		stripR.setPixelColor(i, convertColor(hue[i], 100, 10));		 	
	}
	stripL.show();
	stripR.show();
}

void shiftBtnHueTable(){
	//move all values from the hue table to create a moving effect
	int rembemberValue = hueBtn[0];
	for(int i=0; i<BTNLEDAMOUNT-1; i++){
		
		hueBtn[i] = hueBtn[i+1];
	}
	hueBtn[BTNLEDAMOUNT-1] = rembemberValue;
}

void shiftHueTable(){
	//move all values from the hue table to create a moving effect
	int rembemberValue = hue[0];
	for(int i=0; i<LEDAMOUNT-1; i++){
		
		hue[i] = hue[i+1];
	}
	hue[LEDAMOUNT-1] = rembemberValue;
}

uint32_t convertColor(float hue_, float saturation_, float lightness_){
	//This function converts hsl to rgb
	//to return and store the matching rgb values in one 32bit int

	//Convert hue, sat and lightness to numbers between 0 and 1
	float hueFloat = hue_/360;
	float saturationFloat = saturation_/100;
	float lightnessFloat = lightness_/100;
	float temp1 = 0;
	float temp2 = 0;

	int red = 0;
	int green = 0;
	int blue = 0;

	//Create some temporaryValues for easy calculating
	if(lightnessFloat < 0.5){
		temp1 = lightnessFloat*(1+saturationFloat);
	}
	else{
		temp1 = lightnessFloat+saturationFloat - lightnessFloat*saturationFloat;
	}

	//Create temp2
	temp2 = 2*lightnessFloat - temp1;

	//Create some temporary values for each color channel
	//These Values need to be between 0 and 1(if they're not, Fix it!)
	
	float tempR = hueFloat + 0.333;
	if(tempR >1){
		tempR -=1;
	}
	else if(tempR < 0){
		tempR +=1;
	}

	float tempG = hueFloat;

	float tempB = hueFloat - 0.333;
	if(tempB >1){
		tempB -=1;
	}
	else if(tempB <0){
		tempB +=1;
	}

	//Using the temp values of each channel, we make some tests to select the correct formula
	//RED CHANNEL
	float redFloat = 0;

	if((6*tempR) < 1){
		redFloat = temp2 + (temp1 - temp2)*6*tempR;
	}
	else if(2*tempR < 1){
		redFloat = temp1;
	}
	else if(3*tempR < 2){
		redFloat = temp2 + (temp1 - temp2)*(0.666 -tempR)*6;
	}
	else{
		redFloat = temp2;
	}
	//GREEN CHANNEL
	float greenFloat = 0;

	if((6*tempG) < 1){
		greenFloat = temp2 + (temp1 - temp2)*6*tempG;
	}
	else if(2*tempG < 1){
		greenFloat = temp1;
	}
	else if(3*tempG < 2){
		greenFloat = temp2 + (temp1 - temp2)*(0.666 -tempG)*6;
	}
	else{
		greenFloat = temp2;
	}

	//BLUE CHANNEL
	float blueFloat = 0;

	if((6*tempB) < 1){
		blueFloat = temp2 + (temp1 - temp2)*6*tempB;
	}
	else if(2*tempB < 1){
		blueFloat = temp1;
	}
	else if(3*tempB < 2){
		blueFloat = temp2 + (temp1 - temp2)*(0.666 -tempB)*6;
	}
	else{
		blueFloat = temp2;
	}

	red = redFloat * 255;
	green = greenFloat * 255;
	blue = blueFloat * 255; 

	return combineColors( red, green, blue);
}


uint32_t combineColors( uint32_t red, uint32_t green, uint32_t blue ){
	//Function that bitshifts three 8-bit values into one 32-bit value so you can send one value instead of 3
	//Create a 32-bit unsigned integer (it has room voor 32 bits and cannot go below 0)
	uint32_t oneBigColor = 0;

	oneBigColor = oneBigColor |(red << 16);
	oneBigColor = oneBigColor |(green << 8);
	oneBigColor = oneBigColor |(blue);
	//Serial.println(oneBigColor);
	
	return oneBigColor;
}